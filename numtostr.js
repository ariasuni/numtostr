/* used in threeNumToWordArray */
var units_to_letters = [
    'zéro', 'un', 'deux', 'trois', 'quatre', 'cinq',
    'six', 'sept', 'huit', 'neuf', 'dix',
    'onze', 'douze', 'treize', 'quatorze', 'quinze',
    'seize', 'dix-sept', 'dix-huit', 'dix-neuf'
];
// the two null values are used by the algo to know were to use special rules
var tens_to_letters = [
    'error', 'dix', 'vingt', 'trente', 'quarante', 'cinquante',
    'soixante', null, null, null
];
var NUMBER_RE = /\d+(\.\d*)?/;


// convert a number into a str
// variant can be 'fr', 'be', 'ch' and 'neuvante'
function numToStr(nbr, isOrdinal, variant) {
    nbr = unformat(nbr);
    if(nbr === '') { return 'Vide'; }
    if(NUMBER_RE.test(nbr) == false) { return 'Ce nombre n’existe pas!'; }

    var isNegative = (nbr[0] == '-');
    if(isNegative) {
        nbr = nbr.substring(1, nbr.length);
    }
    nbr = simplify(nbr);

    var isDecimal = (nbr.indexOf('.') != -1);
    if(isOrdinal === undefined) { isOrdinal = false; }

    if(isOrdinal) {
        if(isDecimal) { return 'Les nombres ordinaux à virgule n’existent pas!'; }
        if(isNegative) { return 'Les nombres ordinaux négatifs n’existent pas!'; }
    }

    if(variant == 'be') {
        tens_to_letters[7] = 'septante';
        tens_to_letters[9] = 'nonante';
    } else if(variant == 'ch') {
        tens_to_letters[7] = 'septante';
        tens_to_letters[8] = 'huitante';
        tens_to_letters[9] = 'nonante';
    } else if(variant == 'neuvante') {
        tens_to_letters[7] = 'septante';
        tens_to_letters[8] = 'huitante';
        tens_to_letters[9] = 'neuvante';
    } else { // default ('fr')
        tens_to_letters[7] = null;
        tens_to_letters[8] = 'quatre-vingt';
        tens_to_letters[9] = null;
    }

    var result;
    if(isDecimal) {
        nbr = nbr.toString().split('.');
        result = intToStr(nbr[0]) + ' virgule ' + afterDotToStr(nbr[1]);
    } else {
        result = intToStr(nbr, isOrdinal);
    }
    return (isNegative? 'moins ': '') + result;
}

/* return a str with typographic fantasies removed */
function unformat(nbr) {
    var replace = {
        '−': '-',
        ',': '.',
        ' ': '',
        '\u00A0': '',
        '\u202F': '',
        '_': '',
        '\'': '',
        '’': ''
    };

    if(nbr[0] == '+') { nbr = nbr.substring(1, nbr.length); }

    var replaced, i = 0;
    while(i < nbr.length) {
        replaced = false;
        for(var id in replace) {
            if(nbr[i] == id) {
                nbr = nbr.substring(0, i) + replace[id] + nbr.substring(i + 1);
                replaced = true;
                break;
            }
        }
        if(replaced == false || replace[id] !== '') { ++i; }
    }

    return nbr;
}

/* Try to find a more simple form to a positive integer */
function simplify(nbr) {
    // Remove leading zeros (ex: 0123 -> 123)
    var i = 0;
    while(nbr[i] == '0' && i < nbr.length) { i++; }
    if(i == nbr.length) { return '0'; }
    else { nbr = nbr.substring(i, nbr.length); }

    // Decimal numbers
    if(nbr.indexOf('.') != -1) {
        // Put a zero before leading dot (ex: .8 -> 0.8)
        if(nbr[0] == '.') {
            nbr = '0' + nbr;
        }

        // Remove trailing zeros after the dot (ex: 1.200 -> 1.2, 8.0 -> 8.)
        var i = nbr.length;
        while(nbr[i - 1] == '0') { i--; }
        nbr = nbr.substring(0, i);

        // Remove trailing dot (ex: 8. -> 8)
        if(nbr[nbr.length - 1] == '.') {
            nbr = nbr.substring(0, nbr.length - 1);
        }
    }

    return nbr;
}

function afterDotToStr(nbr) {
    var zeros = '';
    var i = 0;
    while(nbr[i] == '0' && i < nbr.length) { i++; }
    if(i <= 3) {
        for(; i > 0; i--) { zeros += 'zéro-'; }
    } else {
        zeros += intToStr(i.toString()) +' zéros ';
    }
    return zeros + intToStr(nbr);
}

function basicIntToStr(nbr) {
    if(nbr.length == 1 && nbr[0] == '0') { return ['zéro']; }
    var result = [];

    for(var temp, i = -1; nbr.length != 0; i = (i + 1) % 3) {
        temp = parseInt(nbr.substring(nbr.length - 3, nbr.length)); // get last 3 digits
        nbr = nbr.substring(0, nbr.length - 3); // nbr without his last 3 digits

        if(temp != 0) {
            if(i == -1) {}
            else if(i == 0) {
                result.unshift('mille');
            } else {
                if(result.length >= 1) {
                    prev_word = result[0];
                    if(prev_word.indexOf('million') != -1 || prev_word.indexOf('milliard') != -1) {
                        result.unshift('de');
                    }
                }
                if(i == 1) { result.unshift('million'); }
                else       { result.unshift('milliard'); }
            }

            if(temp > 1 && (i == 1 || i == 2)) {
                result[0] += 's';
            }

            result = threeNumToWordArray(temp).concat(result);

            if(i == 0 && result[0] == 'un') {
                result.shift();
            }
        } else if(i == 2) {
            // add 'milliards' when there’s actually only zeros
            if(result.length >= 1) {
                prev_word = result[0];
                if(prev_word.indexOf('million') != -1 ||
                        prev_word.indexOf('milliard') != -1) {
                    result.unshift('de');
                }
            }
            result.unshift('milliards');
        }
    }
    return result;
}

/* convert an integer > 0 into a str */
function intToStr(nbr, isOrdinal) {
    var result = basicIntToStr(nbr);

    if(isOrdinal) {
        last_word = result[result.length - 1];
        if(result.length == 1 && last_word == 'un') {
            last_word = 'premier';
        } else {
            last_letter = last_word[last_word.length - 1];

            if(last_letter == 'e' ||
                    (last_letter == 's' && last_word[0] == 'm')) {
                // quatre/quinze/mille/etc. -> -e, millions/milliards -> -s
                last_word = last_word.substring(0, last_word.length - 1);
            } else if(last_letter == 'f') {
                // neuf -> neuv
                last_word = last_word.substring(0, last_word.length - 1) + 'v';
            } else if(last_letter == 'q') {
                // cinq -> cinqu
                last_word += 'u';
            }

            last_word += 'ième';
        }
        result[result.length - 1] = last_word;
    // quatre-vingt/deux-cent when something after, else quatre-vingts/deux-cents
    } else if(result[result.length - 1] == 'quatre-vingt' ||
            (result[result.length - 1] == 'cent' && result.length > 1)) {
        result[result.length - 1] += 's';
    }

    return result.join('-');
}

/* convert a 0 < number < 1000 into an array of words */
function threeNumToWordArray(nbr) {
    var units = nbr % 10;
    var tens = (nbr % 100 - units) / 10;
    var hundreds = (nbr % 1000 - nbr % 100) / 100;

    var result = []; // will contains all the words to concatenate

    // hundreds
    if(hundreds != 0) {
        // we say «cent», not «un cent»
        if(hundreds != 1) { result.push(units_to_letters[hundreds]); }
        result.push('cent');
    }

    // tens and units
    if(tens == 0 && units == 0) {} // 100 → cent (with nothing more)
    else if(tens < 2) { result.push(units_to_letters[units + 10 * tens]); }
    else {
        var specialCase = (tens_to_letters[tens] == null);

        if(specialCase) {
            // soixante-dix, quatre-vingt-dix
            result.push(tens_to_letters[tens - 1]);
        } else {
            // cinquante, quatre-vingt
            result.push(tens_to_letters[tens]);
        }

        //cinquante/et/un, soixante/et/onze, nonante/et/un
        if(units == 1 && result[result.length - 1].indexOf('quatre-vingt') == - 1) {
            result.push('et');
        }

        if(specialCase) {
            result.push(units_to_letters[units + 10]);
        } else if(units != 0) {
            result.push(units_to_letters[units]);
        }
    }

    return result;
}
